package com.common.io.token;

/**
 * 此类为 mybatis 的占位符处理器 org.apache.ibatis.parsing.TokenHandler
 * 
 * @author Clinton Begin
 */
public interface TokenHandler {
	String handleToken(String content);
}
