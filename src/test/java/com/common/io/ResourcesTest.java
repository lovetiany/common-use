package com.common.io;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.junit.Before;
import org.junit.Test;

public class ResourcesTest {
	
	public static final ClassLoader CLASS_LOADER = Resources.class.getClassLoader();
	
	@Before
	public void before(){
	}
	
	@Test
	public void testGetResourceURL() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceURLClassLoader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsStream() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsStreamClassLoader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsProperties() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsPropertiesClassLoader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsReader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsReaderClassLoader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResourceAsFile() throws IOException {
		// 加载根路径下的 text.txt
//		File file = Resources.getResourceAsFile("text.txt");
//		File file = new File(CLASS_LOADER.getResource("resource.properties").getFile());
		// class 类 getResource() 相对当前类的路径
//		File file = new File(this.getClass().getResource("resource.properties").getFile());
//		this.getClass().getResourceAsStream("resourc.properties");
		BufferedReader reader = new BufferedReader(new InputStreamReader(CLASS_LOADER.getResourceAsStream("resource.properties")));
		String line = null;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
		
		reader.close();
	}

	@Test
	public void testGetResourceAsFileClassLoader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetUrlAsStream() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetUrlAsReader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetUrlAsProperties() {
		fail("Not yet implemented");
	}

	@Test
	public void testClassForName() {
		fail("Not yet implemented");
	}

	@Test
	public void getClassPath() throws Exception {
		
		//当前类的路径
		String otherpath = this.getClass().getResource("").getPath();
		System.out.println("当前类的路径 ： " + otherpath);
		
		//获得项目根目录的绝对路径
		String rootPath = System.getProperty("user.dir");
		System.out.println("user.dir : " + rootPath);
		
		//类路径和包路径
		String classAndPackagePath = System.getProperty("java.class.path");
		System.out.println("class & package path : " + classAndPackagePath);
		
		// 一下三种方式获取的结果是一样的
		String classpath = this.getClass().getResource("/").getPath();
		System.out.println("classpath1 : " + classpath);
		this.getClass().getResourceAsStream("");
		String path = ResourcesTest.class.getResource("/").getPath();
		System.out.println("classpath2 : " + path);
		
		String loaderPath = ResourcesTest.class.getClassLoader().getResource("").getPath();
		System.out.println("classpath3 : " + loaderPath);
		
	}
	
}
